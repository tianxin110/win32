﻿namespace Win32.ComputerInfo
{
    /// <summary>
    /// 处理器
    /// </summary>
    public struct ProcessorInfo
    {
        /// <summary>
        /// 处理器名称
        /// </summary>
        public string Name;
        /// <summary>
        /// 处理器厂商
        /// </summary>
        public string Manufacturer;
        /// <summary>
        /// 处理器序列号
        /// </summary>
        public string SerialNumber;
        /// <summary>
        /// 处理器核心数
        /// </summary>
        public uint NumberOfCores;
        /// <summary>
        /// 处理器逻辑线程数
        /// </summary>
        public uint NumberOfLogicalProcessors;
    }
}
